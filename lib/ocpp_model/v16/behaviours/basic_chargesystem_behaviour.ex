defmodule OcppModel.V16.Behaviours.BasicChargeSystem do
  @moduledoc """
    Behaviour of a Basic ChargeSystem, it has a limited set of actions
  """
  alias OcppModel.V16.Messages, as: M

  @callback authorize(M.AuthorizeRequest.t(), any()) ::
              {{:ok, M.AuthorizeResponse.t()}, any()} | {{:error, :authorize, String.t()}, any()}

  @callback boot_notification(M.BootNotificationRequest.t(), any()) ::
              {{:ok, M.BootNotificationResponse.t()}, any()}
              | {{:error, :boot_notification, String.t()}, any()}

  @callback heartbeat(M.HeartbeatRequest.t(), any()) ::
              {{:ok, M.HeartbeatResponse.t()}, any()} | {{:error, :heartbeat, String.t()}, any()}

  @callback start_transaction(M.StartTransactionRequest.t(), any()) ::
              {{:ok, M.StartTransactionResponse.t()}, any()}
              | {{:error, :start_transaction, String.t()}, any()}

  @callback status_notification(M.StatusNotificationRequest.t(), any()) ::
              {{:ok, M.StatusNotificationResponse.t()}, any()}
              | {{:error, :status_notification, String.t()}, any()}

  @callback stop_transaction(M.StopTransactionRequest.t(), any()) ::
              {{:ok, M.StopTransactionResponse.t()}, any()}
              | {{:error, :stop_transaction, String.t()}, any()}

  defp model_and_method(action, impl) do
    %{
      "Authorize" => {M.AuthorizeRequest, &impl.authorize/2},
      "BootNotification" => {M.BootNotificationRequest, &impl.boot_notification/2},
      "Heartbeat" => {M.HeartbeatRequest, &impl.heartbeat/2},
      "StartTransaction" => {M.StartTransactionRequest, &impl.start_transaction/2},
      "StatusNotification" => {M.StatusNotificationRequest, &impl.status_notification/2},
      "StopTransaction" => {M.StopTransactionRequest, &impl.stop_transaction/2},
    }
    |> Map.get(action)
  end

  @doc """
    Main entrypoint, based on the action parameter, this function will call one of the callback functions with the payload
  """
  @spec handle(atom, String.t(), map, any) :: {{:ok, map}, any} | {{:error, atom, binary}, any}
  def handle(impl, action, payload, state) do
    case model_and_method(action, impl) do
      {model, method} -> OcppModel.execute(payload, model, method, state)
      nil -> {{:error, :unknown_action, "The action #{action} is unknown"}, state}
    end
  end
end
