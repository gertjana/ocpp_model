defmodule OcppModel.V16.DataTypes do
  @moduledoc """
    Field types or subclasses of the OCPP Model
  """

  defmodule IdTagInfo do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :expiryDate, String.t()
      field :parentIdTag, String.t()
      field :status, String.t() # AuthorizationStatus
    end
  end

  defmodule MeterValue do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :timestamp, String.t(), enforce: true
      field :sampledValue, DT.SampledValue.t(), enforce: true
    end
  end

  defmodule SampledValue do
    @moduledoc false
    use TypedStruct

    typedstruct do
      field :value, String.t(), enforce: true
      field :context, String.t() # ReadingContext
      field :format, String.t()
      field :measurand, String.t() # Measurand
      field :phase, String.t() # Phase
      field :location, String.t() # Location
      field :unit, String.t() # UnitOfMeasure
    end
  end
end
