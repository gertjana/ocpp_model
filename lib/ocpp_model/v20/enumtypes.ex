defmodule OcppModel.V20.EnumTypes do
  @moduledoc """
    Contains a map of all EnumTypes that are used in the currently supported messages, with a function to validate if a value is part of the EnumType
  """
  @enum_types %{
    attribute: ["Actual", "Target", "MinSet", "MaxSet"],
    authorizationStatus: [
      "Accepted ",
      "Blocked ",
      "ConcurrentTx",
      "Expired",
      "Invalid",
      "NoCredit",
      "NotAllowedTypeEVSE",
      "NotAtThisLocation",
      "NotAtThisTime",
      "Unknown"
    ],
    authorizeCertificateStatus: [
      "Accepted",
      "SignatureError",
      "CertificateExpired",
      "NoCertificateAvailable",
      "CertChainError",
      "CertificateRevoked",
      "ContractCancelled"
    ],
    bootReason: [
      "ApplicationReset",
      "FirmwareUpdate",
      "LocalReset",
      "PowerUp",
      "RemoteReset",
      "ScheduledReset",
      "Triggered",
      "Unknown",
      "Watchdog"
    ],
    cancelReservationStatus: ["Accepted", "Rejected"],
    certificateAction: ["Install"],
    certificateSignedStatus: ["Accepted", "Rejected"],
    certificateSigningUse: ["ChargingStationCertificate", "V2GCertificate"],
    changeAvailabilityStatus: ["Accepted", "Rejected", "Scheduled"],
    chargingLimitSource: ["EMS", "Other", "SO", "CSO"],
    chargingProfilePurpose: [
      "ChargingStationExternalConstraints",
      "ChargingStationMaxProfile",
      "TxDefaultProfile",
      "TxProfile"
    ],
    chargingProfileKind: ["Absolute", "Recurring", "Relative"],
    chargingProfileStatus: ["Accepted", "Rejected"],
    chargingRateUnit: ["W", "A"],
    chargingState: ["Charging", "SuspendedEV", "SuspendedEVSE", "Idle"],
    clearCacheStatus: ["Accepted", "Rejected"],
    clearChargingProfileStatus: ["Accepted", "Unknown"],
    clearMessageStatus: ["Accepted", "Unknown"],
    clearMonitoringStatus: ["Accepted", "Rejected", "NotFound"],
    componentCriterion: ["Active", "Available", "Enabled", "Problem"],
    connectorStatus: ["Available", "Occupied", "Reserved", "Faulted"],
    connectorType: ["cCCS1", "cCCS2", "cG105", "cTesla", "cType1", "cType2", "s309-1P-16A"],
    costKind: [
      "CarbonDioxideEmission",
      "RelativePricePercentage",
      "RenewableGenerationPercentage"
    ],
    customerInformationStatus: ["Accepted", "Rejected", "Invalid"],
    dataTransferStatus: ["Accepted", "Rejected", "UnknownMessageId", "UnknownVendorId"],
    deleteCertificateStatus: ["Accepted", "Rejected", "NotFound"],
    displayMessageStatus: [
      "Accepted",
      "NotSupportedMessageFormat",
      "NotSupportedPriority",
      "NotSupportedState",
      "Rejected",
      "UnknownTransaction"
    ],
    energyTransferMode: ["DC", "AC_single_phase", "AC_double_phase", "AC_three_phase"],
    eventNotification: [
      "HardWiredNotification",
      "HardWiredMonitor",
      "PreconfiguredMonitor",
      "CustomMonitor"
    ],
    eventTrigger: ["Alerting", "Delta", "Periodic"],
    firmwareStatus: [
      "Downloaded",
      "DownloadFailed",
      "Downloading",
      "DownloadScheduled",
      "DownloadPaused",
      "Idle",
      "InstallationFailed",
      "Installing",
      "Installed",
      "InstallRebooting",
      "InstallScheduled",
      "InstallVerificationFailed",
      "InvalidSignature",
      "SignatureVerified"
    ],
    genericDeviceModelStatus: ["Accepted", "Rejected", "NotSupported", "EmptyResultSet"],
    genericStatus: ["Accepted", "Rejected"],
    getCertificateIdUse: [
      "V2GRootCertificate",
      "MORootCertificate",
      "CSMSRootCertificate",
      "V2GCertificateChain",
      "ManufacturerRootCertificate"
    ],
    getCertificateStatus: ["Accepted", "Failed"],
    getChargingProfileStatus: ["Accepted", "NoProfiles"],
    getDisplayMessagesStatus: ["Accepted", "Unknown"],
    getInstalledCertificateStatus: ["Accepted", "NotFound"],
    getVariableStatus: [
      "Accepted",
      "Rejected",
      "UnknownComponent",
      "UnknownVariable",
      "NotSupportedAttributeType"
    ],
    hashAlgorithm: ["SHA256", "SHA384", "SHA512"],
    idToken: [
      "Central",
      "eMAID",
      "ISO14443",
      "ISO15693",
      "KeyCode",
      "Local",
      "MacAddress ",
      "NoAuthorization"
    ],
    installCertificateStatus: ["Accepted", "Rejected", "Failed"],
    installCertificateUse: [
      "V2GRootCertificate",
      "CSMSRootCertificate",
      "MORootCertificate",
      "ManufacturerRootCertificate"
    ],
    iso15118EVCertificateStatus: ["Accepted", "Failed"],
    location: ["Body", "Cable", "EV", "Inlet", "Outlet"],
    log: ["DiagnosticsLog", "SecurityLog"],
    logStatus: ["Accepted", "Rejected", "AcceptedCanceled"],
    measurand: [
      "Current.Export",
      "Current.Import",
      "Current.Offered",
      "Energy.Active.Export.Register",
      "Energy.Active.Import.Register",
      "Energy.Reactive.Export.Register",
      "Energy.Reactive.Import.Register",
      "Energy.Active.Export.Interval",
      "Energy.Active.Import.Interval",
      "Energy.Active.Net",
      "Energy.Reactive.Export.Interval",
      "Energy.Reactive.Import.Interval",
      "Energy.Reactive.Net",
      "Energy.Apparent.Net",
      "Energy.Apparent.Import",
      "Energy.Apparent.Export",
      "Frequency",
      "Power.Active.Export",
      "Power.Active.Import",
      "Power.Factor",
      "Power.Offered",
      "Power.Reactive.Export",
      "Power.Reactive.Import",
      "SoC",
      "Voltage"
    ],
    messageFormat: ["ASCII", "HTML", "URI", "UTF8"],
    messagePriority: ["AlwaysFront"],
    messageState: ["Charging", "Faulted", "Idle", "Unavailable"],
    messageTrigger: [
      "BootNotification",
      "FirmwareStatusNotification",
      "Heartbeat",
      "LogStatusNotification",
      "MeterValues",
      "PublishFirmwareStatusNotification",
      "SignChargingStationCertificate",
      "SignCombinedCertificate",
      "SignV2GCertificate",
      "StatusNotification",
      "TransactionEvent"
    ],
    monitor: ["UpperThreshold", "LowerThreshold", "Delta", "Periodic", "PeriodicClockAligned"],
    monitoringCriterion: ["ThresholdMonitoring", "DeltaMonitoring", "PeriodicMonitoring"],
    notifyEVChargingNeedsStatus: ["Accepted", "Rejected", "Processing"],
    operationalStatus: ["Inoperative", "Operative"],
    phase: ["L1", "L2", "L3", "N", "L1-N", "L2-N", "L3-N", "L1-L2", "L2-L3", "L3-L1"],
    publishFirmwareStatus: [
      "Idle",
      "DownloadScheduled",
      "Downloading",
      "Downloaded",
      "Published",
      "DownloadFailed",
      "DownloadPaused",
      "InvalidChecksum",
      "ChecksumVerified",
      "PublishFailed"
    ],
    readingContext: [
      "Interruption.Begin",
      "Interruption.End ",
      "Other ",
      "Sample.Clock ",
      "Sample.Periodic"
    ],
    reason: [
      "DeAuthorized",
      "EmergencyStop",
      "EnergyLimitReached",
      "EVDisconnected",
      "GroundFault",
      "ImmediateReset",
      "Local",
      "LocalOutOfCredit",
      "MasterPass",
      "Other",
      "OvercurrentFault",
      "PowerLoss",
      "PowerQuality",
      "Reboot",
      "Remote",
      "SOCLimitReached",
      "StoppedByEV",
      "TimeLimitReached",
      "Timeout"
    ],
    recurrencyKind: ["Daily", "Weekly"],
    registrationStatus: ["Accepted", "Pending", "Rejected"],
    reportBase: ["ConfigurationInventory", "FullInventory", "SummaryInventory"],
    requestStartStopStatus: ["Accepted", "Rejected"],
    reservationUpdateStatus: ["Expired", "Removed"],
    reserveNowStatus: ["Accepted", "Faulted", "Occupied", "Rejected", "Unavailable"],
    reset: ["Immediate", "OnIdle"],
    resetStatus: ["Accepted", "Rejected", "Scheduled"],
    sendLocalListStatus: ["Accepted"],
    transactionEvent: ["Ended", "Started", "Updated"],
    triggerMessageStatus: ["Accepted", "Rejected", "NotImplemented"],
    triggerReason: [
      "Authorized",
      "CablePluggedIn",
      "ChargingRateChanged",
      "ChargingStateChanged",
      "EnergyLimitReached",
      "EVCommunicationLost",
      "EVConnectTimeout",
      "MeterValueClock",
      "MeterValuePeriodic",
      "TimeLimitReached",
      "Trigger",
      "UnlockCommand",
      "StopAuthorized",
      "EVDeparted",
      "EVDetected",
      "RemoteStop",
      "RemoteStart",
      "AbnormalCondition",
      "SignedDataReceived",
      "ResetCommand"
    ],
    update: ["Differential", "Full"],
    uploadLogStatus: [
      "AcceptedCanceled",
      "BadMessage",
      "Idle",
      "NotSupportedOperation",
      "PermissionDenied",
      "Uploaded",
      "UploadFailure",
      "Uploading"
    ],
    unlockStatus: ["Unlocked", "UnlockFailed", "OngoingAuthorizedTransaction", "UnknownConnector"]
  }

  @spec validate?(atom(), String.t()) :: boolean()
  @doc """
    Validates whether or not the value is part of the EnumType
  """
  def validate?(enum_type, value) do
    case Map.get(@enum_types, enum_type) do
      nil -> false
      values -> Enum.member?(values, value)
    end
  end

  @doc """
    Returns the entire EnumTypes Map
  """
  @spec get :: map()
  def get, do: @enum_types

  @doc """
    Returns the list of values for the specified EnumType
  """
  @spec get(atom()) :: list(String.t()) | nil
  def get(item), do: Map.get(@enum_types, item)
end
