defmodule OcppModel.V20.Behaviours.Controller do
  @moduledoc """
    Behaviour for a controller
  """
  alias OcppModel.V20.Messages, as: M

  @callback publish_firmware(M.PublishFirmwareRequest.t(), any()) ::
              {{:ok, %M.PublishFirmwareResponse{}}, any()}
              | {{:error, :publish_firmware, String.t()}, any()}

  defp model_and_method(action, impl) do
    %{
      "PublishFirmware" => {M.PublishFirmwareRequest, &impl.publish_firmware/2}
    } |> Map.get(action)
  end

  @spec handle(atom(), String.t(), map(), any()) ::
          {{:ok, map()}, any()} | {{:error, atom(), String.t()}, any}
  @doc """
    Main entrypoint, based on the action parameter, this function will call one of the callback functions
  """
  def handle(impl, action, payload, state) do
    case model_and_method(action, impl) do
      {model, method} -> OcppModel.execute(payload, model, method, state)
      nil -> {{:error, :unknown_action, "The action #{action} is unknown"}, state}
    end
  end
end
