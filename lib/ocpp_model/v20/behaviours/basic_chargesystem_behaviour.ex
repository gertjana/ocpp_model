defmodule OcppModel.V20.Behaviours.BasicChargeSystem do
  @moduledoc """
    Behaviour of a Basic ChargeSystem, it has a limited set of actions
  """
  alias OcppModel.V20.Messages, as: M

  @callback authorize(M.AuthorizeRequest.t(), any()) ::
              {{:ok, M.AuthorizeResponse.t()}, any()} | {{:error, :authorize, String.t()}, any()}

  @callback boot_notification(M.BootNotificationRequest.t(), any()) ::
              {{:ok, M.BootNotificationResponse.t()}, any()}
              | {{:error, :boot_notification, String.t()}, any()}

  @callback heartbeat(M.HeartbeatRequest.t(), any()) ::
              {{:ok, M.HeartbeatResponse.t()}, any()} | {{:error, :heartbeat, String.t()}, any()}

  @callback status_notification(M.StatusNotificationRequest.t(), any()) ::
              {{:ok, M.StatusNotificationResponse.t()}, any()}
              | {{:error, :status_notification, String.t()}, any()}

  @callback transaction_event(M.TransactionEventRequest.t(), any()) ::
              {{:ok, M.TransactionEventResponse.t()}, any()}
              | {{:error, :transaction_event, String.t()}, any()}

  defp model_and_method(action, impl) do
    %{
      "Authorize" => {M.AuthorizeRequest, &impl.authorize/2},
      "BootNotification" => {M.BootNotificationRequest, &impl.boot_notification/2},
      "Heartbeat" => {M.HeartbeatRequest, &impl.heartbeat/2},
      "StatusNotification" => {M.StatusNotificationRequest, &impl.status_notification/2},
      "TransactionEvent" => {M.TransactionEventRequest, &impl.transaction_event/2}
    }
    |> Map.get(action)
  end

  @doc """
    Main entrypoint, based on the action parameter, this function will call one of the callback functions with the payload
  """
  @spec handle(atom, String.t(), map, any) :: {{:ok, map}, any} | {{:error, atom, binary}, any}
  def handle(impl, action, payload, state) do
    case model_and_method(action, impl) do
      {model, method} -> OcppModel.execute(payload, model, method, state)
      nil -> {{:error, :unknown_action, "The action #{action} is unknown"}, state}
    end
  end
end
