defmodule OcppModel.V20.Behaviours.ChargeSystem do
  @moduledoc """
    Behaviour of a ChargeSystem, allowing the module assuming the behaviour to be able to respond to messages send to it


  """
  alias OcppModel.V20.Messages, as: M

  @callback authorize(M.AuthorizeRequest.t(), any()) ::
              {{:ok, M.AuthorizeResponse.t()}, any()} | {{:error, :authorize, String.t()}, any()}

  @callback boot_notification(M.BootNotificationRequest.t(), any()) ::
              {{:ok, M.BootNotificationResponse.t()}, any()}
              | {{:error, :boot_notification, String.t()}, any()}

  @callback cleared_charging_limit(M.ClearedChargingLimitRequest.t(), any()) ::
              {{:ok, M.ClearedChargingLimitResponse.t()}, any()}
              | {{:error, :cleared_charging_limit, String.t()}, any()}

  @callback data_transfer(M.DataTransferRequest.t(), any()) ::
              {{:ok, M.DataTransferResponse.t()}, any()}
              | {{:error, :data_transfer, String.t()}, any()}

  @callback firmware_status_notification(M.FirmwareStatusNotificationRequest.t(), any()) ::
              {{:ok, M.FirmwareStatusNotificationResponse.t()}, any()}
              | {{:error, :firmware_status_notification, String.t()}, any()}

  @callback get_15118_ev_certificate(M.Get15118EVCertificateRequest.t(), any()) ::
              {{:ok, M.Get15118EVCertificateResponse.t()}, any()}
              | {{:error, :get_15118_ev_certificate, String.t()}, any()}

  @callback get_certificate_status(M.GetCertificateStatusRequest.t(), any()) ::
              {{:ok, M.GetCertificateStatusResponse.t()}, any()}
              | {{:error, :get_certificate_status, String.t()}, any()}

  @callback heartbeat(M.HeartbeatRequest.t(), any()) ::
              {{:ok, M.HeartbeatResponse.t()}, any()} | {{:error, :heartbeat, String.t()}, any()}

  @callback log_status_notification(M.LogStatusNotificationRequest.t(), any()) ::
              {{:ok, M.LogStatusNotificationResponse.t()}, any()}
              | {{:error, :log_status_notification, String.t()}, any()}

  @callback notify_charging_limit(M.NotifyChargingLimitRequest.t(), any()) ::
              {{:ok, M.NotifyChargingLimitResponse.t()}, any()}
              | {{:error, :notify_charging_limit, String.t()}, any()}

  @callback notify_customer_information(M.NotifyCustomerInformationRequest.t(), any()) ::
              {{:ok, M.NotifyCustomerInformationResponse.t()}, any()}
              | {{:error, :notify_customer_information, String.t()}, any()}

  @callback notify_display_messages(M.NotifyDisplayMessagesRequest.t(), any()) ::
              {{:ok, M.NotifyDisplayMessagesResponse.t()}, any()}
              | {{:error, :notify_display_messages, String.t()}, any()}

  @callback notify_ev_charging_needs(M.NotifyEVChargingNeedsRequest.t(), any()) ::
              {{:ok, M.NotifyEVChargingNeedsResponse.t()}, any()}
              | {{:error, :notify_ev_charging_needs, String.t()}, any()}

  @callback notify_ev_charging_schedule(M.NotifyEVChargingScheduleRequest.t(), any()) ::
              {{:ok, M.NotifyEVChargingScheduleResponse.t()}, any()}
              | {{:error, :notify_ev_charging_needs, String.t()}, any()}

  @callback notify_event(M.NotifyEventRequest.t(), any()) ::
              {{:ok, M.NotifyEventResponse.t()}, any()}
              | {{:error, :notify_event, String.t()}, any()}

  @callback notify_monitoring_report(M.NotifyMonitoringReportRequest.t(), any()) ::
              {{:ok, M.NotifyMonitoringReportResponse.t()}, any()}
              | {{:error, :notify_monitoring_report, String.t()}, any()}

  @callback notify_report(M.NotifyReportRequest.t(), any()) ::
              {{:ok, M.NotifyReportResponse.t()}, any()}
              | {{:error, :notify_report, String.t()}, any()}

  @callback meter_values(M.MeterValuesRequest.t(), any()) ::
              {{:ok, M.MeterValuesResponse.t()}, any()}
              | {{:error, :meter_values, String.t()}, any()}

  @callback publish_firmware_status_notification(
              M.PublishFirmwareStatusNotificationRequest.t(),
              any()
            ) ::
              {{:ok, M.PublishFirmwareStatusNotificationResponse.t()}, any()}
              | {{:error, :publish_firmware_status_notification, String.t()}, any()}

  @callback report_charging_profiles(M.ReportChargingProfilesRequest.t(), any()) ::
              {{:ok, M.ReportChargingProfilesResponse.t()}, any()}
              | {{:error, :report_charging_profiles, String.t()}, any()}

  @callback reservation_status_update(M.ReservationStatusUpdateRequest.t(), any()) ::
              {{:ok, M.ReservationStatusUpdateResponse.t()}, any()}
              | {{:error, :reservation_status_update, String.t()}, any()}

  @callback security_event_notification(M.SecurityEventNotificationRequest.t(), any()) ::
              {{:ok, M.SecurityEventNotificationResponse.t()}, any()}
              | {{:error, :security_event_notification, String.t()}, any()}

  @callback status_notification(M.StatusNotificationRequest.t(), any()) ::
              {{:ok, M.StatusNotificationResponse.t()}, any()}
              | {{:error, :status_notification, String.t()}, any()}

  @callback transaction_event(M.TransactionEventRequest.t(), any()) ::
              {{:ok, M.TransactionEventResponse.t()}, any()}
              | {{:error, :transaction_event, String.t()}, any()}

  defp model_and_method(action, impl) do
    %{
      "Authorize" => {M.AuthorizeRequest, &impl.authorize/2},
      "BootNotification" => {M.BootNotificationRequest, &impl.boot_notification/2},
      "ClearedChargingLimit" => {M.ClearedChargingLimitRequest, &impl.cleared_charging_limit/2},
      "DataTransfer" => {M.DataTransferRequest, &impl.data_transfer/2},
      "FirmwareStatusNotification" =>
        {M.FirmwareStatusNotificationRequest, &impl.firmware_status_notification/2},
      "Get15118EVCertificate" => {M.Get15118EVCertificateRequest, &impl.get_15118_ev_certificate/2},
      "GetCertificateStatus" => {M.GetCertificateStatusRequest, &impl.get_certificate_status/2},
      "Heartbeat" => {M.HeartbeatRequest, &impl.heartbeat/2},
      "LogStatusNotification" => {M.LogStatusNotificationRequest, &impl.log_status_notification/2},
      "MeterValues" => {M.MeterValuesRequest, &impl.meter_values/2},
      "NotifyChargingLimit" => {M.NotifyChargingLimitRequest, &impl.notify_charging_limit/2},
      "NotifyCustomerInformation" => {M.NotifyCustomerInformationRequest, &impl.meter_values/2},
      "NotifyDisplayMessages" => {M.NotifyDisplayMessagesRequest, &impl.notify_display_messages/2},
      "NotifyEVChargingNeeds" => {M.NotifyEVChargingNeedsRequest, &impl.notify_ev_charging_needs/2},
      "NotifyEVChargingSchedule" =>
        {M.NotifyEVChargingScheduleRequest, &impl.notify_ev_charging_schedule/2},
      "NotifyEvent" => {M.NotifyEventRequest, &impl.notify_event/2},
      "NotifyMonitoringReport" =>
        {M.NotifyMonitoringReportRequest, &impl.notify_monitoring_report/2},
      "NotifyReport" => {M.NotifyReportRequest, &impl.notify_report/2},
      "PublishFirmwareStatusNotification" =>
        {M.PublishFirmwareStatusNotificationRequest, &impl.status_notification/2},
      "ReportChargingProfiles" =>
        {M.ReportChargingProfilesRequest, &impl.report_charging_profiles/2},
      "ReservationStatusUpdate" =>
        {M.ReservationStatusUpdateRequest, &impl.reservation_status_update/2},
      "SecurityEventNotification" =>
        {M.SecurityEventNotificationRequest, &impl.security_event_notification/2},
      "StatusNotification" => {M.StatusNotificationRequest, &impl.status_notification/2},
      "TransactionEvent" => {M.TransactionEventRequest, &impl.transaction_event/2}
    }
    |> Map.get(action)
  end

  @spec handle(atom(), String.t(), map(), any()) ::
          {{:ok, map()}, any} | {{:error, atom(), String.t()}, any}
  @doc """
    Main entrypoint, based on the action parameter, this function will call one of the callback functions with the payload
  """
  def handle(impl, action, payload, state) do
    case model_and_method(action, impl) do
      {model, method} -> OcppModel.execute(payload, model, method, state)
      nil -> {{:error, :unknown_action, "The action #{action} is unknown"}, state}
    end
  end
end
