#!/bin/bash
Y=`grep -i "| yes" messages_$1.md | wc -l`
N=`grep -i "| no" messages_$1.md | wc -l`
T=`echo "scale=2;${Y}+${N}" | bc`
R=`echo "scale=2;100*(${Y}/${T})" | bc`
echo "${Y} of ${T} ocpp $1 objects implemented (${R} %)"