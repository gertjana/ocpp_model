defmodule OcppModel.V16.ChargerTest do
  use ExUnit.Case

  alias Implementations.V16.MyTestBasicCharger

  @state %{}

  test "MyTestCharger.handle method should give a CallResult response when a Reset Call message is given" do
    message = [2, "42", "Reset", %{type: "Hard"}]
    expected = [3, "42", %{status: "Accepted"}]
    assert {expected, %{}} == MyTestBasicCharger.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallError response when a incorrect Call message is given" do
    message = [2, "42", "Unknown", %{}]
    expected = [4, "42", "unknown_action", "The action Unknown is unknown", {}]
    assert {expected, %{}} == MyTestBasicCharger.handle(message, @state)
  end
end
