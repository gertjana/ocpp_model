defmodule OcppModel.V16.MessagePropertiesTest do
  @compile :nowarn_deprecated_type
  @moduledoc """
    Property based testing for the OCPP Message Structs, for now just checks if the correct structs are generated,
    in the future they will be used to fire off these messages to both the charger and the backend

  """
  use ExUnit.Case
  use ExUnitProperties

  alias OcppModel.V16.Messages, as: M
  import Generators.V16.Messages

  property :authorize_request do
    check(
      all(
        gen_auth_req <- authorize_request_generator(),
        do: assert(%M.AuthorizeRequest{} = gen_auth_req)
      )
    )
  end

  property :authorize_response do
    check(
      all(
        gen_auth_res <- authorize_response_generator(),
        do: assert(%M.AuthorizeResponse{} = gen_auth_res)
      )
    )
  end

  property :boot_notification_request do
    check(
      all(
        gen_boot_req <- boot_notification_request_generator(),
        do: assert(%M.BootNotificationRequest{} = gen_boot_req)
      )
    )
  end

  property :boot_notification_response do
    check(
      all(
        gen_boot_res <- boot_notification_response_generator(),
        do: assert(%M.BootNotificationResponse{} = gen_boot_res)
      )
    )
  end

  property :heartbeat_request do
    check(
      all(
        gen_hb_req <- heartbeat_request_generator(),
        do: assert(%M.HeartbeatRequest{} = gen_hb_req)
      )
    )
  end

  property :heartbeat_response do
    check(
      all(
        gen_hb_res <- heartbeat_response_generator(),
        do: assert(%M.HeartbeatResponse{} = gen_hb_res)
      )
    )
  end

  property :start_transaction_request do
    check all gen_st_req <- start_transaction_request_generator(),
      do: assert %M.StartTransactionRequest{} = gen_st_req
  end

  property :start_transaction_response do
    check all gen_st_res <- start_transaction_response_generator(),
      do: assert %M.StartTransactionResponse{} =  gen_st_res
  end

  property :status_notification_request do
    check all gen_sn_req <- status_notification_request_generator(),
      do: assert %M.StatusNotificationRequest{} = gen_sn_req
  end

  property :status_notification_response do
    check all gen_sn_res <- status_notification_response_generator(),
      do: assert %M.StatusNotificationResponse{} = gen_sn_res
  end

  property :stop_transaction_request do
    check all gen_st_req <- stop_transaction_request_generator(),
      do: assert %M.StopTransactionRequest{} = gen_st_req
  end

  property :stop_transaction_response do
    check all gen_st_res <- stop_transaction_response_generator(),
      do: assert %M.StopTransactionResponse{} = gen_st_res
  end

  property :reset_request do
    check(
      all(
        gen_reset_req <- reset_request_generator(),
        do: assert(%M.ResetRequest{} = gen_reset_req)
      )
    )
  end

  property :reset_response do
    check(
      all(
        gen_reset_res <- reset_response_generator(),
        do: assert(%M.ResetResponse{} = gen_reset_res)
      )
    )
  end

end
