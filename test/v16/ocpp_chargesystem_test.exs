defmodule OcppModel.V16.ChargeSystemTest do
  use ExUnit.Case

  alias Implementations.V16.MyTestBasicChargeSystem

  @state %{}

  test "MyTestChargeSystem.handle Authorize Call" do
    message = [2, "42", "Authorize", %{idToken: "01020304"}]
    assert {[3, "42", %{idTagInfo: %{status: "Accepted"}}], %{}} == MyTestBasicChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle BootNotification Call" do
    message = [2, "42", "BootNotification", %{
                  chargePointserialNumber: "GA-XC-001",
                  chargepointVendor: "GA",
                  chargePointModel: "PiZeroNerves"
              }]
    assert {[3, "42", %{currentTime: _, interval: 900, status: "Accepted"}], %{}} = MyTestBasicChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle Heartbeat Call" do
    message = [2, "42", "Heartbeat", %{}]
    assert {[3, "42", %{currentTime: _}], %{}} = MyTestBasicChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle StartTransaction Call" do
    message = [2, "42", "StartTransaction", %{connectorId: 1, idTag: "01020304", meterStart: 1337, timestamp: "2021-01-01T00:00:00Z"}]
    assert {[3, "42", %{idTagInfo: %{status: "Accepted"}, transactionId: 42}], %{}} = MyTestBasicChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle StatusNotification Call" do
  message = [2, "42", "StatusNotification", %{connectorId: 1, errorCode: "NoError", status: "Preparing"}]
    assert {[3, "42", %{}], %{}} = MyTestBasicChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle StopTransaction Call" do
    message = [2, "42", "StopTransaction", %{meterStop: 7331, timestamp: "2021-01-01T00:00:00Z", transactionId: 42}]
      assert {[3, "42", %{}], %{}} = MyTestBasicChargeSystem.handle(message, @state)
    end

  test "MyTestChargeSystem.handle an incorrect Call" do
    message = [2, "42", "Unknown", %{}]

    assert {[4, "42", "unknown_action", "The action Unknown is unknown", {}], %{}} ==
             MyTestBasicChargeSystem.handle(message, @state)
  end
end
