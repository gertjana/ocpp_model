
ExUnit.start()

Code.compiler_options(ignore_module_conflict: true)
Code.require_file("test/generators/generic_v16.ex")
Code.require_file("test/generators/datatypes_v16.ex")
Code.require_file("test/generators/messages_v16.ex")
Code.require_file("test/implementations/v16/my_test_basic_charger.ex")
Code.require_file("test/implementations/v16/my_test_basic_chargesystem.ex")

Code.require_file("test/generators/generic_v20.ex")
Code.require_file("test/generators/datatypes_v20.ex")
Code.require_file("test/generators/messages_v20.ex")
Code.require_file("test/implementations/v20/my_test_charger.ex")
Code.require_file("test/implementations/v20/my_test_chargesystem.ex")
Code.require_file("test/implementations/v20/my_test_basic_charger.ex")
Code.require_file("test/implementations/v20/my_test_basic_chargesystem.ex")
Code.require_file("test/implementations/v20/my_test_controller.ex")
