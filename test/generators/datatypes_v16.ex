defmodule Generators.V16.DataTypes do
  @moduledoc false

  use ExUnitProperties

  alias OcppModel.V16.DataTypes, as: DT

  import Generators.V16.Generic

  def id_tag_info_generator do
    gen all gen_expiry_date <- date_time_generator(),
            gen_parent_id_tag <- string_generator(20),
            gen_status <- enum_generator(:authorizationStatus) do
      %DT.IdTagInfo{
        expiryDate: gen_expiry_date,
        parentIdTag: gen_parent_id_tag,
        status: gen_status
      }
    end
  end

  def meter_value_generator do
    gen all gen_ts <- date_time_generator(),
            gen_value <- sampled_value_generator() do
      %DT.MeterValue{
        timestamp: gen_ts,
        sampledValue: gen_value
      }
    end
  end

  def sampled_value_generator do
    gen all gen_value <- string_generator(50),
            gen_context <- enum_generator(:readingContext),
            gen_format <- enum_generator(:valueFormat),
            gen_measurand <- enum_generator(:measurand),
            gen_phase <- enum_generator(:phase),
            gen_location <- enum_generator(:location),
            gen_unit <- enum_generator(:unitOfMeasure) do
      %DT.SampledValue{
        value: gen_value,
        context: gen_context,
        format: gen_format,
        measurand: gen_measurand,
        phase: gen_phase,
        location: gen_location,
        unit: gen_unit
      }
    end

  end

end
