defmodule Generators.V16.Generic do
  @moduledoc false

  use ExUnitProperties

  alias OcppModel.V16.EnumTypes, as: ET
  alias StreamData, as: SD

  def integer_generator, do: SD.integer()

  def integer_generator(range), do: SD.integer(range)

  def boolean_generator, do: SD.boolean()

  def binary_generator, do: SD.binary()

  def list_generator(list), do: SD.list_of(list)

  def list_generator(list, opt), do: SD.list_of(list, opt)

  def string_generator(length, opt \\ :alphanumeric) do
    gen(
      all(str <- SD.string(opt), str != "", snipped = String.slice(str, 0..length), do: snipped)
    )
  end

  def float_generator, do: SD.float()

  def enum_generator(enum_type) do
    gen(all(value <- SD.member_of(ET.get(enum_type)), do: value))
  end

  def date_time_generator do
    gen all(
          year <- SD.integer(2000..2020),
          month <- SD.integer(1..12),
          day <- SD.integer(1..28),
          hour <- SD.integer(0..23),
          minute <- SD.integer(0..59),
          second <- SD.integer(0..59)
        ) do
      DateTime.new!(Date.new!(year, month, day), Time.new!(hour, minute, second, 0), "Etc/UTC")
      |> DateTime.to_iso8601()
    end
  end
end
