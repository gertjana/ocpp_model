defmodule Generators.V16.Messages do
  @moduledoc false

  use ExUnitProperties

  alias OcppModel.V16.Messages, as: M

  import Generators.V16.DataTypes
  import Generators.V16.Generic

  def authorize_request_generator do
    gen all(
          gen_id_token <- string_generator(20)
        ) do
      %M.AuthorizeRequest{
        idToken: gen_id_token,
      }
    end
  end

  def authorize_response_generator do
    gen all(
          gen_id_tag_info <- id_tag_info_generator()
        ) do
      %M.AuthorizeResponse{
        idTagInfo: gen_id_tag_info
      }
    end
  end

  def boot_notification_request_generator do
    gen all gen_box_serial <- string_generator(25),
            gen_cp_model <- string_generator(20),
            gen_cp_serial <- string_generator(25),
            gen_cp_vendor <- string_generator(20),
            gen_fw_ver <- string_generator(50),
            gen_iccid <- string_generator(20),
            gen_imsi <- string_generator(20),
            gen_meter_serial <- string_generator(25),
            gen_meter_type <- string_generator(25) do
      %M.BootNotificationRequest{
        chargeBoxSerialNumber: gen_box_serial,
        chargePointModel: gen_cp_model,
        chargePointSerialNumber: gen_cp_serial,
        chargePointVendor: gen_cp_vendor,
        firmwareVersion: gen_fw_ver,
        iccid: gen_iccid,
        imsi: gen_imsi,
        meterSerialNumber: gen_meter_serial,
        meterType: gen_meter_type
      }
    end
  end

  def boot_notification_response_generator do
    gen all gen_time <- date_time_generator(),
            gen_interval <- integer_generator(),
            gen_status <- enum_generator(:registrationStatus) do
      %M.BootNotificationResponse{
        currentTime: gen_time,
        interval: gen_interval,
        status: gen_status
      }
    end
  end

  def heartbeat_request_generator do
    gen all _ <- nil do
      %M.HeartbeatRequest{}
    end
  end

  def heartbeat_response_generator do
    gen all gen_time <- date_time_generator() do
      %M.HeartbeatResponse{
        currentTime: gen_time
      }
    end
  end

  def start_transaction_request_generator do
    gen all gen_con_id <- integer_generator(),
            gen_id_tag <- string_generator(20),
            gen_meter_start <- integer_generator(),
            gen_res_id <- integer_generator(),
            gen_ts <- date_time_generator() do
      %M.StartTransactionRequest{
        connectorId: gen_con_id,
        idTag: gen_id_tag,
        meterStart: gen_meter_start,
        reservactionId: gen_res_id,
        timestamp: gen_ts
      }
    end
  end

  def start_transaction_response_generator do
    gen all gen_id_tag_info <- id_tag_info_generator(),
            gen_tr_id <- integer_generator() do
      %M.StartTransactionResponse{
        idTagInfo: gen_id_tag_info,
        transactionId: gen_tr_id
      }
    end
  end

  def status_notification_request_generator do
    gen all gen_con_id <- integer(),
            gen_err_code <- enum_generator(:chargePointErrorCode),
            gen_info <- string_generator(50),
            gen_status <- enum_generator(:chargePointStatus),
            gen_ts <- date_time_generator(),
            gen_vendor_id <- string_generator(25),
            gen_vendor_err <- string_generator(50) do
      %M.StatusNotificationRequest{
        connectorId: gen_con_id,
        errorCode: gen_err_code,
        info: gen_info,
        status: gen_status,
        timestamp: gen_ts,
        vendorId: gen_vendor_id,
        vendorErrorCode: gen_vendor_err
      }
    end
  end

  def status_notification_response_generator do
    gen all _ <- nil do
      %M.StatusNotificationResponse{}
    end
  end

  def stop_transaction_request_generator do
    gen all gen_id_tag <- string_generator(20),
            gen_meter_stop <- integer_generator(),
            gen_ts <- date_time_generator(),
            gen_tr_id <- integer_generator(),
            gen_reason <- enum_generator(:reason),
            gen_tr_data <- meter_value_generator() do
      %M.StopTransactionRequest{
        idTag: gen_id_tag,
        meterStop: gen_meter_stop,
        timestamp: gen_ts,
        transactionId: gen_tr_id,
        reason: gen_reason,
        transactionData: gen_tr_data
      }
    end
  end

  def stop_transaction_response_generator do
    gen all gen_id_tag_info <- id_tag_info_generator() do
      %M.StopTransactionResponse{
        idTagInfo: gen_id_tag_info
      }
    end
  end

  def reset_request_generator do
    gen all gen_type <- enum_generator(:resetType) do
      %M.ResetRequest{
        type: gen_type
      }
    end
  end

  def reset_response_generator do
    gen all gen_status <- enum_generator(:resetStatus) do
      %M.ResetResponse{
        status: gen_status
      }
    end
  end
end
