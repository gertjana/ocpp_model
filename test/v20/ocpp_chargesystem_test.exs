defmodule OcppModel.V20.ChargeSystemTest do
  use ExUnit.Case

  alias Implementations.V20.MyTestBasicChargeSystem
  alias Implementations.V20.MyTestChargeSystem

  @state %{}

  @tr_ev_request %{
    eventType: "Started",
    timestamp: DateTime.now!("Etc/UTC") |> DateTime.to_iso8601(),
    triggerReason: "Authorized",
    seqNo: 0,
    transactionInfo: %{transactionId: "GA-XC-001_1"},
    evse: %{id: 1, connector_id: 2},
    meterValue: %{
      timestamp: DateTime.now!("Etc/UTC") |> DateTime.to_iso8601(),
      sampledValue: %{
        value: 12.34,
        signedMeterValue: %{
          signedMeterData: "blabla",
          signingMethod: "SHA256",
          encodingMethod: "GZ",
          publicKey: "something public something"
        },
        unitOfMeasure: %{
          unit: "kWh",
          multiplier: 0
        }
      }
    }
  }
  @boot_not_request %{
    reason: "PowerUp",
    chargingStation: %{
      serialNumber: "GA-XC-001",
      vendorName: "GA",
      model: "XC"
    }
  }
  @auth_request %{
    idToken: %{
      idToken: "",
      type: "NoAuthorization",
      additionalInfo: %{
        additionalIdToken: "123456",
        type: "OTP"
      }
    }
  }
  @meter_value_request %{
    evseId: 0,
    meterValue: %{
      timestamp: DateTime.now!("Etc/UTC") |> DateTime.to_iso8601(),
      sampledValue: %{
        value: 12.34,
        signedMeterValue: %{
          signedMeterData: "blabla",
          signingMethod: "SHA256",
          encodingMethod: "GZ",
          publicKey: "something public something"
        },
        unitOfMeasure: %{
          unit: "kWh",
          multiplier: 0
        }
      }
    }
  }
  @cleared_limit_request %{
    chargingLimitSource: "CSO",
    evseId: 1
  }

  test "MyTestChargeSystem.handle should give a CallResult response when a correct Authorize Call message is given" do
    message = [2, "42", "Authorize", @auth_request]
    assert {[3, "42", %{idTokenInfo: %{status: "Accepted"}}], %{}} == MyTestChargeSystem.handle(message, @state)
    assert {[3, "42", %{idTokenInfo: %{status: "Accepted"}}], %{}} == MyTestBasicChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct BootNotification Call message is given" do
    message = [2, "42", "BootNotification", @boot_not_request]
    assert {[3, "42", %{currentTime: _}], %{}} = MyTestChargeSystem.handle(message, @state)
    assert {[3, "42", %{currentTime: _}], %{}} = MyTestBasicChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct ClearedChargingLimit Call message is given" do
    message = [2, "42", "ClearedChargingLimit", @cleared_limit_request]
    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct DataTransfer Call message is given" do
    message = [
      2,
      "42",
      "DataTransfer",
      %{messageId: "001", data: "All your base are belong to us", vendorId: "GA"}
    ]

    assert {[3, "42", %{status: "Accepted", data: "su ot gnoleb era esab ruoy llA"}], %{}} =
             MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct FirmwareStatusNotification Call message is given" do
    message = [2, "42", "FirmwareStatusNotification", %{status: "Downloaded", requestId: 1337}]
    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct Get15118EVCertificate Call message is given" do
    message = [
      2,
      "42",
      "Get15118EVCertificate",
      %{
        iso15118SchemaVersion: "",
        action: "Install",
        exiRequest: "some base64 encoded exi certificate stream"
      }
    ]
    assert {[3, "42", %{status: "Accepted", exiResponse: "some base64 encoded exi certificate stream"}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct GetCertificateStatus Call message is given" do
    message = [2, "42", "GetCertificateStatus",
      %{
        ocspRequestData: %{
          hashAlgorithm: "SHA",
          issuerNameHash: "some_hashed_name",
          issuerKeyHash: "some_hashed_key",
          serialNumber: "0123456",
          responderURL: "some_responder_url"
        }
      }
    ]

    assert {[3, "42", %{status: "Accepted"}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct Heartbeat Call message is given" do
    message = [2, "42", "Heartbeat", %{}]
    assert {[3, "42", %{currentTime: _}], %{}} = MyTestChargeSystem.handle(message, @state)
    assert {[3, "42", %{currentTime: _}], %{}} = MyTestBasicChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct LogStatusNotification Call message is given" do
    message = [2, "42", "LogStatusNotification", %{status: "Idle"}]
    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct MeterValues Call message is given" do
    message = [2, "42", "MeterValues", @meter_value_request]
    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct NotifyChargingLimit Call message is given" do
    message = [2, "42", "NotifyChargingLimit", %{chargingLimit: %{charginglimitSource: "EMS"}}]
    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct NotifyCustomerInformation Call message is given" do
    message = [
      2,
      "42",
      "NotifyCustomerInformation",
      %{data: "some data", seqNo: 1, generatedAt: "2021-01-01T03:14:15:924Z", requestId: 73}
    ]

    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct NotifyDisplayMessages Call message is given" do
    message = [
      2,
      "42",
      "NotifyDisplayMessages",
      %{evseId: 1, chargingNeeds: %{requestedEnergyTransfer: "DC"}}
    ]

    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct NotifyEVChargingNeeds Call message is given" do
    message = [2, "42", "NotifyEVChargingNeeds", %{requestId: 73}]
    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct NotifyEVChargingSchedule Call message is given" do
    message = [
      2,
      "42",
      "NotifyEVChargingSchedule",
      %{timeBase: "2020-01-01T01:00:00.000Z", evseId: 1, chargingSchedule: %{id: 1337}}
    ]

    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct NotifyEvent Call message is given" do
    message = [
      2,
      "42",
      "NotifyEvent",
      %{
        generatedAt: "2020-01-01T01:00:00.000Z",
        seqNo: 73,
        eventData: %{
          eventId: 73,
          timestamp: "2020-01-01T01:00:00.000Z",
          trigger: "Delta",
          actualValue: "Some Value",
          eventNotificationType: "HardWiredMonitor",
          component: %{name: "SomeComponent"},
          variable: %{name: "SomeVariable"}
        }
      }
    ]

    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct NotifyMonitoringReport Call message is given" do
    message = [
      2,
      "42",
      "NotifyMonitoringReport",
      %{requestId: 73, seqNo: 1, generatedAt: "2020-01-01T01:00:00.000Z"}
    ]

    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct NotifyReport Call message is given" do
    message = [
      2,
      "42",
      "NotifyReport",
      %{requestId: 73, generatedAt: "2020-01-01T01:00:00.000Z", seqNo: 1}
    ]

    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct PublishFirmwareStatusNotification Call message is given" do
    message = [2, "42", "PublishFirmwareStatusNotification", %{status: "Downloading"}]
    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct ReportChargingProfiles Call message is given" do
    message = [
      2,
      "42",
      "ReportChargingProfiles",
      %{
        requestId: 73,
        chargingLimitSource: "EMS",
        evseId: 1,
        chargingProfile: %{
          id: 42,
          stackLevel: 1,
          chargingProfilePurpose: "TxProfile",
          chargingProfileKind: "Absolute"
        }
      }
    ]

    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct ReservationStatusUpdate Call message is given" do
    message = [
      2,
      "42",
      "ReservationStatusUpdate",
      %{reservationId: 1337, reservationUpdateStatus: "Expired"}
    ]

    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct SecurityEventNotification Call message is given" do
    message = [
      2,
      "42",
      "SecurityEventNotification",
      %{type: "CabinetOpened", timestamp: "2020-01-01T01:00:00.000Z"}
    ]

    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct StatusNotification Call message is given" do
    message = [
      2,
      "42",
      "StatusNotification",
      %{
        timestamp: DateTime.now!("Etc/UTC") |> DateTime.to_iso8601(),
        connectorStatus: "Available",
        evseId: 0,
        connectorId: 0
      }
    ]

    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
    assert {[3, "42", %{}], %{}} = MyTestBasicChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallResult response when a correct TransactionEvent Call message is given" do
    message = [2, "42", "TransactionEvent", @tr_ev_request]
    assert {[3, "42", %{}], %{}} = MyTestChargeSystem.handle(message, @state)
    assert {[3, "42", %{}], %{}} = MyTestBasicChargeSystem.handle(message, @state)
  end

  test "MyTestChargeSystem.handle should give a CallError response when a incorrect Call message is given" do
    message = [2, "42", "Unknown", %{}]

    assert {[4, "42", "unknown_action", "The action Unknown is unknown", {}], %{}} ==
             MyTestChargeSystem.handle(message, @state)

    assert {[4, "42", "unknown_action", "The action Unknown is unknown", {}], %{}} ==
             MyTestBasicChargeSystem.handle(message, @state)
  end
end
