defmodule OcppModel.V20.MessagePropertiesTest do
  @compile :nowarn_deprecated_type
  @moduledoc """
    Property based testing for the OCPP Message Structs, for now just checks if the correct structs are generated,
    in the future they will be used to fire off these messages to both the charger and the backend

  """
  use ExUnit.Case
  use ExUnitProperties

  alias OcppModel.V20.Messages, as: M
  import Generators.V20.Messages

  property :authorize_request do
    check(
      all(
        gen_auth_req <- authorize_request_generator(),
        do: assert(%M.AuthorizeRequest{} = gen_auth_req)
      )
    )
  end

  property :authorize_response do
    check(
      all(
        gen_auth_res <- authorize_response_generator(),
        do: assert(%M.AuthorizeResponse{} = gen_auth_res)
      )
    )
  end

  property :boot_notification_request do
    check(
      all(
        gen_boot_req <- bootnotification_request_generator(),
        do: assert(%M.BootNotificationRequest{} = gen_boot_req)
      )
    )
  end

  property :boot_notification_response do
    check(
      all(
        gen_boot_res <- bootnotification_response_generator(),
        do: assert(%M.BootNotificationResponse{} = gen_boot_res)
      )
    )
  end

  property :cancel_reservation_request do
    check(
      all(
        gen_cancel_reservation_req <- cancel_reservation_request_generator(),
        do: assert(%M.CancelReservationRequest{} = gen_cancel_reservation_req)
      )
    )
  end

  property :cancel_reservation_response do
    check(
      all(
        gen_cancel_reservation_res <- cancel_reservation_response_generator(),
        do: assert(%M.CancelReservationResponse{} = gen_cancel_reservation_res)
      )
    )
  end

  property :certificate_signed_request do
    check(
      all(
        gen_cert_signed_req <- certificate_signed_request_generator(),
        do: assert(%M.CertificateSignedRequest{} = gen_cert_signed_req)
      )
    )
  end

  property :certificate_signed_response do
    check(
      all(
        gen_cert_signed_res <- certificate_signed_response_generator(),
        do: assert(%M.CertificateSignedResponse{} = gen_cert_signed_res)
      )
    )
  end

  property :clear_cache_request do
    check(
      all(
        gen_clear_cache_req <- clear_cache_request_generator(),
        do: assert(%M.ClearCacheRequest{} = gen_clear_cache_req)
      )
    )
  end

  property :clear_cache_response do
    check(
      all(
        gen_clear_cache_res <- clear_cache_response_generator(),
        do: assert(%M.ClearCacheResponse{} = gen_clear_cache_res)
      )
    )
  end

  property :clear_charging_profile_request do
    check(
      all(
        gen_clear_charging_profile_request <- clear_charging_profile_request_generator(),
        do: %M.ClearChargingProfileRequest{} = gen_clear_charging_profile_request
      )
    )
  end

  property :clear_charging_profile_response do
    check(
      all(
        gen_clear_charging_profile_response <- clear_charging_profile_response_generator(),
        do: %M.ClearChargingProfileResponse{} = gen_clear_charging_profile_response
      )
    )
  end

  property :clear_display_message_request do
    check(
      all(
        gen_clear_display_message_req <- clear_display_message_request_generator(),
        do: %M.ClearDisplayMessageRequest{} = gen_clear_display_message_req
      )
    )
  end

  property :clear_display_message_response do
    check(
      all(
        gen_clear_display_message_res <- clear_display_message_response_generator(),
        do: %M.ClearDisplayMessageResponse{} = gen_clear_display_message_res
      )
    )
  end

  property :cleared_charging_limit_request do
    check(
      all(
        gen_cleared_limit_req <- cleared_charging_limit_request_generator(),
        do: %M.ClearedChargingLimitRequest{} = gen_cleared_limit_req
      )
    )
  end

  property :cleared_charging_limit_response do
    check(
      all(
        gen_cleared_limit_res <- cleared_charging_limit_response_generator(),
        do: %M.ClearedChargingLimitResponse{} = gen_cleared_limit_res
      )
    )
  end

  property :clear_var_monitoring_request do
    check(
      all(
        gen_var_mon_req <- clear_var_monitoring_request_generator(),
        do: %M.ClearVariableMonitoringRequest{} = gen_var_mon_req
      )
    )
  end

  property :clear_var_monitoring_response do
    check(
      all(
        gen_var_mon_res <- clear_var_monitoring_response_generator(),
        do: %M.ClearVariableMonitoringResponse{} = gen_var_mon_res
      )
    )
  end

  property :cost_updated_request do
    check(
      all(
        gen_cost_updated_req <- cost_updated_request_generator(),
        do: %M.CostUpdatedRequest{} = gen_cost_updated_req
      )
    )
  end

  property :cost_updated_response do
    check(
      all(
        gen_cost_updated_res <- cost_updated_response_generator(),
        do: %M.CostUpdatedResponse{} = gen_cost_updated_res
      )
    )
  end

  property :customer_information_request do
    check(
      all(
        gen_cust_info_req <- customer_information_request_generator(),
        do: %M.CustomerInformationRequest{} = gen_cust_info_req
      )
    )
  end

  property :customer_information_response do
    check(
      all(
        gen_cust_info_res <- customer_information_response_generator(),
        do: %M.CustomerInformationResponse{} = gen_cust_info_res
      )
    )
  end

  property :data_transfer_request do
    check(
      all(
        gen_data_transfer_req <- datatransfer_request_generator(),
        do: %M.DataTransferRequest{} = gen_data_transfer_req
      )
    )
  end

  property :data_transfer_response do
    check(
      all(
        gen_data_transfer_res <- datatransfer_response_generator(),
        do: %M.DataTransferResponse{} = gen_data_transfer_res
      )
    )
  end

  property :delete_certificate_hash_data_request do
    check(
      all(
        gen_delete_cert_hash <- delete_certificate_request_generator(),
        do: %M.DeleteCertificateRequest{} = gen_delete_cert_hash
      )
    )
  end

  property :delete_certificate_hash_data_response do
    check(
      all(
        gen_delete_cert_hash <- delete_certificate_response_generator(),
        do: %M.DeleteCertificateResponse{} = gen_delete_cert_hash
      )
    )
  end

  property :firmware_status_notification_request do
    check(
      all(
        gen_fw_status_not_req <- firmware_status_notification_request_generator(),
        do: %M.FirmwareStatusNotificationRequest{} = gen_fw_status_not_req
      )
    )
  end

  property :firmware_status_notification_response do
    check(
      all(
        gen_fw_status_not_res <- firmware_status_notification_response_generator(),
        do: %M.FirmwareStatusNotificationResponse{} = gen_fw_status_not_res
      )
    )
  end

  property :get_15118_ev_certificate_request do
    check(
      all(
        gen_cert_req <- get_15118_ev_certificate_request_generator(),
        do: %M.Get15118EVCertificateRequest{} = gen_cert_req
      )
    )
  end

  property :get_15118_ev_certificate_response do
    check(
      all(
        gen_cert_res <- get_15118_ev_certificate_response_generator(),
        do: %M.Get15118EVCertificateResponse{} = gen_cert_res
      )
    )
  end

  property :get_base_report_request do
    check(
      all(
        gen_base_report_req <- get_base_report_request_generator(),
        do: %M.GetBaseReportRequest{} = gen_base_report_req
      )
    )
  end

  property :get_base_report_response do
    check(
      all(
        gen_base_report_res <- get_base_report_response_generator(),
        do: %M.GetBaseReportResponse{} = gen_base_report_res
      )
    )
  end

  property :get_certificate_status_request do
    check(
      all(
        gen_get_cert_status_req <- get_certificate_status_request_generator(),
        do: %M.GetCertificateStatusRequest{} = gen_get_cert_status_req
      )
    )
  end

  property :get_certificate_status_response do
    check(
      all(
        gen_get_cert_status_res <- get_certificate_status_response_generator(),
        do: %M.GetCertificateStatusResponse{} = gen_get_cert_status_res
      )
    )
  end

  property :get_charging_profiles_request do
    check(
      all(
        gen_get_profile_req <- get_charging_profile_request_generator(),
        do: %M.GetChargingProfilesRequest{} = gen_get_profile_req
      )
    )
  end

  property :get_charging_profiles_response do
    check(
      all(
        gen_get_profile_res <- get_charging_profile_response_generator(),
        do: %M.GetChargingProfilesResponse{} = gen_get_profile_res
      )
    )
  end

  property :get_composite_schedule_request do
    check(
      all(
        gen_comp_schedule_req <- get_composite_schedule_request_generator(),
        do: %M.GetCompositeScheduleRequest{} = gen_comp_schedule_req
      )
    )
  end

  property :get_composite_schedule_response do
    check(
      all(
        gen_comp_schedule_res <- get_composite_schedule_response_generator(),
        do: %M.GetCompositeScheduleResponse{} = gen_comp_schedule_res
      )
    )
  end

  property :get_local_list_version_request do
    check(
      all(
        gen_ll_version_req <- get_local_list_version_request_generator(),
        do: %M.GetLocalListVersionRequest{} = gen_ll_version_req
      )
    )
  end

  property :get_local_list_version_response do
    check(
      all(
        gen_ll_version_res <- get_local_list_version_response_generator(),
        do: %M.GetLocalListVersionResponse{} = gen_ll_version_res
      )
    )
  end

  property :get_display_messages_request do
    check(
      all(
        gen_display_messages_req <- get_display_messages_request_generator(),
        do: %M.GetDisplayMessagesRequest{} = gen_display_messages_req
      )
    )
  end

  property :get_display_messages_response do
    check(
      all(
        gen_display_messages_res <- get_display_messages_response_generator(),
        do: %M.GetDisplayMessagesResponse{} = gen_display_messages_res
      )
    )
  end

  property :get_installed_certificate_ids_request do
    check(
      all(
        gen_get_installed_cert_req <- get_installed_certificate_ids_request_generator(),
        do: %M.GetInstalledCertificateIdsRequest{} = gen_get_installed_cert_req
      )
    )
  end

  property :get_installed_certificate_ids_response do
    check(
      all(
        gen_get_installed_cert_res <- get_installed_certificate_ids_response_generator(),
        do: %M.GetInstalledCertificateIdsResponse{} = gen_get_installed_cert_res
      )
    )
  end

  property :get_log_request do
    check(
      all(
        gen_get_log_req <- get_log_request_generator(),
        do: %M.GetLogRequest{} = gen_get_log_req
      )
    )
  end

  property :get_log_response do
    check(
      all(
        gen_get_log_res <- get_log_response_generator(),
        do: %M.GetLogResponse{} = gen_get_log_res
      )
    )
  end

  property :get_monitoring_report_request do
    check(
      all(
        gen_get_mon_report_req <- get_monitoring_report_request_generator(),
        do: %M.GetMonitoringReportRequest{} = gen_get_mon_report_req
      )
    )
  end

  property :get_monitoring_report_response do
    check(
      all(
        gen_get_mon_report_res <- get_monitoring_report_response_generator(),
        do: %M.GetMonitoringReportResponse{} = gen_get_mon_report_res
      )
    )
  end

  property :get_report_request do
    check(
      all(
        gen_get_report_req <- get_report_request_generator(),
        do: %M.GetReportRequest{} = gen_get_report_req
      )
    )
  end

  property :get_report_response do
    check(
      all(
        gen_get_report_res <- get_report_response_generator(),
        do: %M.GetReportResponse{} = gen_get_report_res
      )
    )
  end

  property :get_transaction_status_request do
    check(
      all(
        gen_get_trans_status_req <- get_transaction_status_request_generator(),
        do: %M.GetTransactionStatusRequest{} = gen_get_trans_status_req
      )
    )
  end

  property :get_transaction_status_response do
    check(
      all(
        gen_get_trans_status_res <- get_transaction_status_response_generator(),
        do: %M.GetTransactionStatusResponse{} = gen_get_trans_status_res
      )
    )
  end

  property :get_variables_request do
    check(
      all(
        gen_get_var_req <- get_variables_request_generator(),
        do: %M.GetVariablesRequest{} = gen_get_var_req
      )
    )
  end

  property :get_variables_response do
    check(
      all(
        gen_get_var_res <- get_variables_response_generator(),
        do: %M.GetVariablesResponse{} = gen_get_var_res
      )
    )
  end

  property :heartbeat_request do
    check(
      all(
        gen_hb_req <- heartbeat_request_generator(),
        do: assert(%M.HeartbeatRequest{} = gen_hb_req)
      )
    )
  end

  property :heartbeat_response do
    check(
      all(
        gen_hb_res <- heartbeat_response_generator(),
        do: assert(%M.HeartbeatResponse{} = gen_hb_res)
      )
    )
  end

  property :install_certificate_request do
    check(
      all(
        gen_cert_req <- install_certificate_request_generator(),
        do: assert(%M.InstallCertificateRequest{} = gen_cert_req)
      )
    )
  end

  property :install_certificate_response do
    check(
      all(
        gen_cert_res <- install_certificate_response_generator(),
        do: assert(%M.InstallCertificateResponse{} = gen_cert_res)
      )
    )
  end

  property :log_status_notification_request do
    check(
      all(
        gen_log_status_not_req <- log_status_notification_request_generator(),
        do: %M.LogStatusNotificationRequest{} = gen_log_status_not_req
      )
    )
  end

  property :log_status_notification_response do
    check(
      all(
        gen_log_status_not_res <- log_status_notification_response_generator(),
        do: %M.LogStatusNotificationResponse{} = gen_log_status_not_res
      )
    )
  end

  property :meter_values_request do
    check(
      all(
        gen_mv_req <- meter_values_request_generator(),
        do: assert(%M.MeterValuesRequest{} = gen_mv_req)
      )
    )
  end

  property :meter_values_response do
    check(
      all(
        gen_mv_res <- meter_values_response_generator(),
        do: assert(%M.MeterValuesResponse{} = gen_mv_res)
      )
    )
  end

  property :notify_charging_limit_request do
    check(
      all(
        gen_not_limit_req <- notify_charging_limit_request_generator(),
        do: assert(%M.NotifyChargingLimitRequest{} = gen_not_limit_req)
      )
    )
  end

  property :notify_charging_limit_response do
    check(
      all(
        gen_not_limit_res <- notify_charging_limit_response_generator(),
        do: assert(%M.NotifyChargingLimitResponse{} = gen_not_limit_res)
      )
    )
  end

  property :notify_customer_information_request do
    check(
      all(
        gen_not_cust_info_req <- notify_customer_information_request_generator(),
        do: assert(%M.NotifyCustomerInformationRequest{} = gen_not_cust_info_req)
      )
    )
  end

  property :notify_customer_information_response do
    check(
      all(
        gen_not_cust_info_res <- notify_customer_information_response_generator(),
        do: assert(%M.NotifyCustomerInformationResponse{} = gen_not_cust_info_res)
      )
    )
  end

  property :notify_display_messages_request do
    check(
      all(
        gen_not_disp_mes_req <- notify_display_messages_request_generator(),
        do: %M.NotifyDisplayMessagesRequest{} = gen_not_disp_mes_req
      )
    )
  end

  property :notify_display_messages_response do
    check(
      all(
        gen_not_disp_mes_res <- notify_display_messages_response_generator(),
        do: %M.NotifyDisplayMessagesResponse{} = gen_not_disp_mes_res
      )
    )
  end

  property :notify_ev_charging_needs_request do
    check(
      all(
        gen_not_ev_needs_req <- notify_ev_charging_needs_request_generator(),
        do: %M.NotifyEVChargingNeedsRequest{} = gen_not_ev_needs_req
      )
    )
  end

  property :notify_ev_charging_needs_response do
    check(
      all(
        gen_not_ev_needs_res <- notify_ev_charging_needs_response_generator(),
        do: %M.NotifyEVChargingNeedsResponse{} = gen_not_ev_needs_res
      )
    )
  end

  property :notify_ev_charging_schedule_request do
    check(
      all(
        gen_not_ev_charging_schedule_req <- notify_ev_charging_schedule_request_generator(),
        do: %M.NotifyEVChargingScheduleRequest{} = gen_not_ev_charging_schedule_req
      )
    )
  end

  property :notify_ev_charging_schedule_response do
    check(
      all(
        gen_not_ev_charging_schedule_res <- notify_ev_charging_schedule_response_generator(),
        do: %M.NotifyEVChargingScheduleResponse{} = gen_not_ev_charging_schedule_res
      )
    )
  end

  property :notify_event_request do
    check(
      all(
        gen_not_ev_req <- notify_event_request_generator(),
        do: %M.NotifyEventRequest{} = gen_not_ev_req
      )
    )
  end

  property :notify_event_response do
    check(
      all(
        gen_not_ev_res <- notify_event_response_generator(),
        do: %M.NotifyEventResponse{} = gen_not_ev_res
      )
    )
  end

  property :notify_monitor_report_request do
    check(
      all(
        gen_not_mon_req <- notify_monitoring_report_request_generator(),
        do: %M.NotifyMonitoringReportRequest{} = gen_not_mon_req
      )
    )
  end

  property :notify_monitor_report_response do
    check(
      all(
        gen_not_mon_res <- notify_monitoring_report_response_generator(),
        do: %M.NotifyMonitoringReportResponse{} = gen_not_mon_res
      )
    )
  end

  property :notify_report_request do
    check(
      all(
        gen_not_rep_req <- notify_report_request_generator(),
        do: %M.NotifyReportRequest{} = gen_not_rep_req
      )
    )
  end

  property :notify_report_response do
    check(
      all(
        gen_not_rep_res <- notify_report_response_generator(),
        do: %M.NotifyReportResponse{} = gen_not_rep_res
      )
    )
  end

  property :publish_firmware_request do
    check(
      all(
        gen_pub_fw_req <- publish_firmware_request_generator(),
        do: %M.PublishFirmwareRequest{} = gen_pub_fw_req
      )
    )
  end

  property :publish_firmware_response do
    check(
      all(
        gen_pub_fw_res <- publish_firmware_response_generator(),
        do: %M.PublishFirmwareResponse{} = gen_pub_fw_res
      )
    )
  end

  property :publish_firmware_status_notification_request do
    check(
      all(
        gen_pub_fw_not_req <- publish_firmware_status_notification_request_generator(),
        do: %M.PublishFirmwareStatusNotificationRequest{} = gen_pub_fw_not_req
      )
    )
  end

  property :publish_firmware_status_notification_response do
    check(
      all(
        gen_pub_fw_not_res <- publish_firmware_status_notification_response_generator(),
        do: %M.PublishFirmwareStatusNotificationResponse{} = gen_pub_fw_not_res
      )
    )
  end

  property :report_charging_profiles_request do
    check(
      all(
        gen_rep_ch_pro_req <- report_charging_profiles_request_generator(),
        do: %M.ReportChargingProfilesRequest{} = gen_rep_ch_pro_req
      )
    )
  end

  property :report_charging_profiles_response do
    check(
      all(
        gen_rep_ch_pro_res <- report_charging_profiles_response_generator(),
        do: %M.ReportChargingProfilesResponse{} = gen_rep_ch_pro_res
      )
    )
  end

  property :request_transaction_start_request do
    check(
      all(
        gen_req_trans_start_req <- request_start_transaction_request_generator(),
        do: %M.RequestStartTransactionRequest{} = gen_req_trans_start_req
      )
    )
  end

  property :request_transaction_start_response do
    check(
      all(
        gen_req_trans_start_res <- request_start_transaction_response_generator(),
        do: %M.RequestStartTransactionResponse{} = gen_req_trans_start_res
      )
    )
  end

  property :request_stop_transaction_request do
    check(
      all(
        gen_req_stop_req <- request_stop_transaction_request_generator(),
        do: %M.RequestStopTransactionRequest{} = gen_req_stop_req
      )
    )
  end

  property :request_stop_transaction_response do
    check(
      all(
        gen_req_stop_res <- request_stop_transaction_response_generator(),
        do: %M.RequestStopTransactionResponse{} = gen_req_stop_res
      )
    )
  end

  property :reservation_status_update_request do
    check(
      all(
        gen_res_status_update_req <- reservation_status_update_request_generator(),
        do: %M.ReservationStatusUpdateRequest{} = gen_res_status_update_req
      )
    )
  end

  property :reservation_status_update_response do
    check(
      all(
        gen_res_status_update_res <- reservation_status_update_response_generator(),
        do: %M.ReservationStatusUpdateResponse{} = gen_res_status_update_res
      )
    )
  end

  property :reserve_now_request do
    check(
      all(
        gen_reserve_now_req <- reserve_now_request_generator(),
        do: %M.ReserveNowRequest{} = gen_reserve_now_req
      )
    )
  end

  property :reserve_now_response do
    check(
      all(
        gen_reserve_now_res <- reserve_now_response_generator(),
        do: %M.ReserveNowResponse{} = gen_reserve_now_res
      )
    )
  end

  property :reset_request do
    check(
      all(
        gen_reset_req <- reset_request_generator(),
        do: assert(%M.ResetRequest{} = gen_reset_req)
      )
    )
  end

  property :reset_response do
    check(
      all(
        gen_reset_res <- reset_response_generator(),
        do: assert(%M.ResetResponse{} = gen_reset_res)
      )
    )
  end

  property :security_event_notification_request do
    check(
      all(
        gen_sec_evt_not_req <- security_event_notification_request_generator(),
        do: assert(%M.SecurityEventNotificationRequest{} = gen_sec_evt_not_req)
      )
    )
  end

  property :security_event_notification_response do
    check(
      all(
        gen_sec_evt_not_res <- security_event_notification_response_generator(),
        do: assert(%M.SecurityEventNotificationResponse{} = gen_sec_evt_not_res)
      )
    )
  end

  property :send_local_list_request do
    check(
      all(
        gen_send_ll_req <- send_local_list_request_generator(),
        do: %M.SendLocalListRequest{} = gen_send_ll_req
      )
    )
  end

  property :send_local_list_response do
    check(
      all(
        gen_send_ll_res <- send_local_list_response_generator(),
        do: %M.SendLocalListResponse{} = gen_send_ll_res
      )
    )
  end

  property :set_charging_profile_request do
    check(
      all(
        gen_set_profile_req <- set_charging_profile_request_generator(),
        do: %M.SetChargingProfileRequest{} = gen_set_profile_req
      )
    )
  end

  property :set_charging_profile_response do
    check(
      all(
        gen_set_profile_res <- set_charging_profile_response_generator(),
        do: %M.SetChargingProfileResponse{} = gen_set_profile_res
      )
    )
  end

  property :set_display_message_request do
    check(
      all(
        gen_set_disp_mes_req <- set_display_message_request_generator(),
        do: %M.SetDisplayMessageRequest{} = gen_set_disp_mes_req
      )
    )
  end

  property :set_display_message_response do
    check(
      all(
        gen_set_disp_mes_res <- set_display_message_response_generator(),
        do: %M.SetDisplayMessageResponse{} = gen_set_disp_mes_res
      )
    )
  end

  property :status_notification_request do
    check(
      all(
        gen_st_not_req <- status_notification_request_generator(),
        do: assert(%M.StatusNotificationRequest{} = gen_st_not_req)
      )
    )
  end

  property :status_notification_response do
    check(
      all(
        gen_st_not_res <- status_notification_response_generator(),
        do: assert(%M.StatusNotificationResponse{} = gen_st_not_res)
      )
    )
  end

  property :transaction_event_request do
    check(
      all(
        gen_trans_event_req <- transactionevent_request_generator(),
        do: assert(%M.TransactionEventRequest{} = gen_trans_event_req)
      )
    )
  end

  property :transaction_event_response do
    check(
      all(
        gen_trans_event_res <- transactionevent_response_generator(),
        do: assert(%M.TransactionEventResponse{} = gen_trans_event_res)
      )
    )
  end

  property :trigger_message_request do
    check(
      all(
        gen_trigger_req <- trigger_message_request_generator(),
        do: assert(%M.TriggerMessageRequest{} = gen_trigger_req)
      )
    )
  end

  property :trigger_message_response do
    check(
      all(
        gen_trigger_res <- trigger_message_response_generator(),
        do: assert(%M.TriggerMessageResponse{} = gen_trigger_res)
      )
    )
  end
end
