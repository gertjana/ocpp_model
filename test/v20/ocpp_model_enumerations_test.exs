defmodule OcppModel.V20.EnumerationsTest do
  use ExUnit.Case

  alias OcppModel.V20.EnumTypes, as: ET

  test "valid enumeration" do
    assert ET.validate?(:triggerReason, "MeterValuePeriodic")
  end

  test "wrong key enumeration" do
    assert !ET.validate?(:unknown, "Accepted")
  end

  test "wrong value enumeration" do
    assert !ET.validate?(:operationalStatus, "CertificateExpired")
  end

  test "get all enum types" do
    assert %{} = ET.get()
  end

  test "get a single enum type" do
    assert ["SHA256", "SHA384", "SHA512"] == ET.get(:hashAlgorithm)
  end

  test "get an unknown enum type" do
    assert nil == ET.get(:foo)
  end
end
