defmodule OcppModel.V20.ControllerTest do
  use ExUnit.Case

  alias Implementations.V20.MyTestController

  @state %{}

  test "MyController.handle correctly handles PublishFirmware" do
    message = [
      2,
      "42",
      "PublishFirmware",
      %{location: "ftps://firmware.location", checksum: "MD5_checksum", requestId: 73}
    ]

    assert {[3, "42", %{status: "Accepted"}], %{}} == MyTestController.handle(message, @state)
  end

  test "MyTestCharger.handle method should give a CallError response when a incorrect Call message is given" do
    message = [2, "42", "Unknown", %{}]
    assert {[4, "42", "unknown_action", "The action Unknown is unknown", {}], %{}} == MyTestController.handle(message, @state)
  end
end
