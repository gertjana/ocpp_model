defmodule Implementations.V16.MyTestBasicCharger do
    @moduledoc """
      Basic charger implementation, only supports the minimum required to do a chargesession.
    """

    alias OcppModel.V16.Behaviours, as: B
    # alias OcppModel.V16.DataTypes, as: DT
    # alias OcppModel.V16.EnumTypes, as: ET
    alias OcppModel.V16.Messages, as: M

    @behaviour B.BasicCharger

    def handle([2, id, action, payload], state) do
      case B.BasicCharger.handle(__MODULE__, action, payload, state) do
        {{:ok, response_payload}, new_state} -> {[3, id, response_payload], new_state}
        {{:error, error, desc}, new_state} -> {[4, id, Atom.to_string(error), desc, {}], new_state}
      end
    end

    def handle({[3, id, payload], _state}),
      do: IO.puts("Received answer for id #{id}: #{inspect(payload)}")

    def handle({[4, id, err, desc, det], _state}),
      do: IO.puts("Received error for id #{id}: #{err}, #{desc}, #{det}")

    @impl B.BasicCharger
    def reset(_req, state) do
      {{:ok, %M.ResetResponse{status: "Accepted"}}, state}
    end
end
