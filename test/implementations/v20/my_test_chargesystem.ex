defmodule Implementations.V20.MyTestChargeSystem do
  @moduledoc """
    Chargesystem implementation, support any implemented message in the library
  """

  alias OcppModel.V20.Behaviours, as: B
  alias OcppModel.V20.DataTypes, as: DT
  alias OcppModel.V20.EnumTypes, as: ET
  alias OcppModel.V20.Messages, as: M

  @behaviour B.ChargeSystem

  def handle([2, id, action, payload], state) do
    case B.ChargeSystem.handle(__MODULE__, action, payload, state) do
      {{:ok, response_payload}, new_state} -> {[3, id, response_payload], new_state}
      {{:error, error, desc}, new_state} -> {[4, id, Atom.to_string(error), desc, {}], new_state}
    end
  end

  def handle([3, id, payload], _state),
    do: IO.puts("Received answer for id #{id}: #{inspect(payload)}")

  def handle([4, id, err, desc, det], _state),
    do: IO.puts("Received error for id #{id}: #{err}, #{desc}, #{det}")

  @impl B.ChargeSystem
  def authorize(_req, state) do
    {{:ok, %M.AuthorizeResponse{idTokenInfo: %DT.IdTokenInfo{status: "Accepted"}}}, state}
  end

  @impl B.ChargeSystem
  def boot_notification(req, state) do
    if ET.validate?(:bootReason, req.reason) do
      {{:ok,
        %M.BootNotificationResponse{
          currentTime: current_time(),
          interval: 900,
          status: %DT.StatusInfo{reasonCode: ""}
        }}, state}
    else
      {{:error, :boot_notification, "#{req.reason} is not a valid BootReason"}, state}
    end
  end

  @impl B.ChargeSystem
  def cleared_charging_limit(_req, state) do
    {{:ok, %M.ClearedChargingLimitResponse{}}, state}
  end

  @impl B.ChargeSystem
  def data_transfer(req, state) do
    case req.vendorId do
      "GA" ->
        {{:ok, %M.DataTransferResponse{status: "Accepted", data: String.reverse(req.data)}},
         state}

      _ ->
        {{:ok, %M.DataTransferResponse{status: "UnknownVendorId"}}, state}
    end
  end

  @impl B.ChargeSystem
  def firmware_status_notification(_req, state) do
    {{:ok, %M.FirmwareStatusNotificationResponse{}}, state}
  end

  @impl B.ChargeSystem
  def get_15118_ev_certificate(req, state) do
    {{:ok, %M.Get15118EVCertificateResponse{status: "Accepted", exiResponse: req.exiRequest}},
     state}
  end

  @impl B.ChargeSystem
  def get_certificate_status(_req, state) do
    {{:ok, %M.GetCertificateStatusResponse{status: "Accepted"}}, state}
  end

  @impl B.ChargeSystem
  def heartbeat(_req, state) do
    {{:ok, %M.HeartbeatResponse{currentTime: current_time()}}, state}
  end

  @impl B.ChargeSystem
  def log_status_notification(_req, state) do
    {{:ok, %M.LogStatusNotificationResponse{}}, state}
  end

  @impl B.ChargeSystem
  def meter_values(_req, state) do
    {{:ok, %M.MeterValuesResponse{}}, state}
  end

  @impl B.ChargeSystem
  def notify_charging_limit(_req, state) do
    {{:ok, %M.NotifyChargingLimitResponse{}}, state}
  end

  @impl B.ChargeSystem
  def notify_customer_information(_req, state) do
    {{:ok, %M.NotifyCustomerInformationResponse{}}, state}
  end

  @impl B.ChargeSystem
  def notify_display_messages(_req, state) do
    {{:ok, %M.NotifyDisplayMessagesResponse{}}, state}
  end

  @impl B.ChargeSystem
  def notify_ev_charging_needs(_req, state) do
    {{:ok, %M.NotifyEVChargingNeedsResponse{status: "Accepted"}}, state}
  end

  @impl B.ChargeSystem
  def notify_ev_charging_schedule(_req, state) do
    {{:ok, %M.NotifyEVChargingScheduleResponse{}}, state}
  end

  @impl B.ChargeSystem
  def notify_event(_req, state) do
    {{:ok, %M.NotifyEventResponse{}}, state}
  end

  @impl B.ChargeSystem
  def notify_monitoring_report(_req, state) do
    {{:ok, %M.NotifyMonitoringReportResponse{}}, state}
  end

  @impl B.ChargeSystem
  def notify_report(_req, state) do
    {{:ok, %M.NotifyReportResponse{}}, state}
  end

  @impl B.ChargeSystem
  def publish_firmware_status_notification(_req, state) do
    {{:ok, %M.PublishFirmwareStatusNotificationResponse{}}, state}
  end

  @impl B.ChargeSystem
  def report_charging_profiles(_req, state) do
    {{:ok, %M.ReportChargingProfilesResponse{}}, state}
  end

  @impl B.ChargeSystem
  def reservation_status_update(_req, state) do
    {{:ok, %M.ReservationStatusUpdateResponse{}}, state}
  end

  @impl B.ChargeSystem
  def status_notification(_req, state) do
    {{:ok, %M.StatusNotificationResponse{}}, state}
  end

  @impl B.ChargeSystem
  def security_event_notification(_req, state) do
    {{:ok, %M.SecurityEventNotificationResponse{}}, state}
  end

  @impl B.ChargeSystem
  def transaction_event(_req, state) do
    {{:ok, %M.TransactionEventResponse{}}, state}
  end

  def current_time, do: DateTime.now!("Etc/UTC") |> DateTime.to_iso8601()
end
