defmodule Implementations.V20.MyTestCharger do
  @moduledoc """
    Charger implementation, support any implemented message in the library
  """

  alias OcppModel.V20.Behaviours, as: B
  alias OcppModel.V20.DataTypes, as: DT
  alias OcppModel.V20.EnumTypes, as: ET
  alias OcppModel.V20.Messages, as: M

  @behaviour B.Charger

  def handle([2, id, action, payload], state) do
    case B.Charger.handle(__MODULE__, action, payload, state) do
      {{:ok, response_payload}, new_state} -> {[3, id, response_payload], new_state}
      {{:error, error, desc}, new_state} -> {[4, id, Atom.to_string(error), desc, {}], new_state}
    end
  end

  def handle([3, id, payload], _state),
    do: IO.puts("Received answer for id #{id}: #{inspect(payload)}")

  def handle([4, id, err, desc, det], _state),
    do: IO.puts("Received error for id #{id}: #{err}, #{desc}, #{det}")

  @impl B.Charger
  def cancel_reservation(_req, state) do
    {{:ok, %M.CancelReservationResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def certificate_signed(_req, state) do
    {{:ok, %M.CertificateSignedResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def change_availability(req, state) do
    if ET.validate?(:operationalStatus, req.operationalStatus) do
      {{:ok,
        %M.ChangeAvailabilityResponse{
          status: "Accepted",
          statusInfo: %DT.StatusInfo{reasonCode: "charger is inoperative"}
        }}, state}
    else
      {{:error, :change_availabiity, "#{req.operationalStatus} is not a valid OperationalStatus"},
       state}
    end
  end

  @impl B.Charger
  def clear_cache(_req, state) do
    {{:ok, %M.ClearCacheResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def clear_charging_profile(_req, state) do
    {{:ok, %M.ClearChargingProfileResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def clear_display_message(_req, state) do
    {{:ok, %M.ClearDisplayMessageResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def clear_variable_monitoring(req, state) do
    results = req.id |> Enum.map(fn i -> %DT.ClearMonitoringResult{status: "Accepted", id: i} end)

    {{:ok,
      %M.ClearVariableMonitoringResponse{
        clearMonitoringResult: results
      }}, state}
  end

  @impl B.Charger
  def cost_updated(_req, state) do
    {{:ok, %M.CostUpdatedResponse{}}, state}
  end

  @impl B.Charger
  def customer_information(_req, state) do
    {{:ok, %M.CustomerInformationResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def data_transfer(_req, state), do: {{:ok, %M.DataTransferResponse{status: "Accepted"}}, state}

  @impl B.Charger
  def delete_certificate(_req, state) do
    {{:ok,
      %M.DeleteCertificateResponse{
        status: "Accepted",
        statusInfo: %DT.StatusInfo{reasonCode: "cert deleted"}
      }}, state}
  end

  @impl B.Charger
  def get_base_report(_req, state) do
    {{:ok, %M.GetBaseReportResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def get_charging_profiles(_req, state) do
    {{:ok, %M.GetChargingProfilesResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def get_composite_schedule(_req, state) do
    {{:ok, %M.GetCompositeScheduleResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def get_display_messages(_req, state) do
    {{:ok, %M.GetDisplayMessagesResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def get_installed_certificate_ids(_req, state) do
    {{:ok, %M.GetInstalledCertificateIdsResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def get_local_list_version(_req, state) do
    {{:ok, %M.GetLocalListVersionResponse{versionNumber: 73}}, state}
  end

  @impl B.Charger
  def get_log(_req, state) do
    {{:ok, %M.GetLogResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def get_monitoring_report(_req, state) do
    {{:ok, %M.GetMonitoringReportResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def get_report(_req, state) do
    {{:ok, %M.GetReportResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def get_transaction_status(_req, state) do
    {{:ok,
      %M.GetTransactionStatusResponse{
        ongoingIndicator: false,
        messagesInQueue: false
      }}, state}
  end

  @impl B.Charger
  def get_variables(_req, state) do
    {{:ok,
      %M.GetVariablesResponse{
        getVariableResult: [
          %DT.GetVariableResult{
            attributeStatus: "Accepted",
            component: %DT.Component{name: "Foo"}
          }
        ]
      }}, state}
  end

  @impl B.Charger
  def install_certificate(_req, state) do
    {{:ok, %M.InstallCertificateResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def request_start_transaction(_req, state) do
    {{:ok, %M.RequestStartTransactionResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def request_stop_transaction(_req, state) do
    {{:ok, %M.RequestStopTransactionResponse{}}, state}
  end

  @impl B.Charger
  def reserve_now(_req, state) do
    {{:ok, %M.ReserveNowResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def reset(req, state) do
    if ET.validate?(:reset, req.type) do
      case req.type do
        "Immediate" -> {{:ok, %M.ResetResponse{status: "Accepted"}}, state}
        "OnIdle" -> {{:ok, %M.ResetResponse{status: "Scheduled"}}, state}
      end
    else
      {{:error, "Unknown Reset Type #{req.type}"}, state}
    end
  end

  @impl B.Charger
  def send_local_list(_req, state) do
    {{:ok, %M.SendLocalListResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def set_charging_profile(_req, state) do
    {{:ok, %M.SetChargingProfileResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def set_display_message(_req, state) do
    {{:ok, %M.SetDisplayMessageResponse{status: "Accepted"}}, state}
  end

  @impl B.Charger
  def trigger_message(_req, state) do
    {{:ok, %M.TriggerMessageResponse{status: "NotImplemented"}}, state}
  end

  @impl B.Charger
  def unlock_connector(_req, state),
    do:
      {{:ok,
        %M.UnlockConnectorResponse{
          status: "Unlocked",
          statusInfo: %DT.StatusInfo{reasonCode: "cable unlocked"}
        }}, state}
end
