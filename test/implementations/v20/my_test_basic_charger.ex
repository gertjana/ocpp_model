defmodule Implementations.V20.MyTestBasicCharger do
  @moduledoc """
    Basic charger implementation, only supports the minimum required to do a chargesession.
  """

  alias OcppModel.V20.Behaviours, as: B
  alias OcppModel.V20.DataTypes, as: DT
  alias OcppModel.V20.EnumTypes, as: ET
  alias OcppModel.V20.Messages, as: M

  @behaviour B.BasicCharger

  def handle([2, id, action, payload], state) do
    case B.BasicCharger.handle(__MODULE__, action, payload, state) do
      {{:ok, response_payload}, new_state} -> {[3, id, response_payload], new_state}
      {{:error, error, desc}, new_state} -> {[4, id, Atom.to_string(error), desc, {}], new_state}
    end
  end

  def handle({[3, id, payload], _state}),
    do: IO.puts("Received answer for id #{id}: #{inspect(payload)}")

  def handle({[4, id, err, desc, det], _state}),
    do: IO.puts("Received error for id #{id}: #{err}, #{desc}, #{det}")

  @impl B.BasicCharger
  def reset(req, state) do
    if ET.validate?(:reset, req.type) do
      case req.type do
        "Immediate" -> {{:ok, %M.ResetResponse{status: "Accepted"}}, state}
        "OnIdle" -> {{:ok, %M.ResetResponse{status: "Scheduled"}}, state}
      end
    else
      {{:error, "Unknown Reset Type #{req.type}"}, state}
    end
  end

  @impl B.BasicCharger
  def unlock_connector(_req, state),
    do:
      {{:ok,
        %M.UnlockConnectorResponse{
          status: "Unlocked",
          statusInfo: %DT.StatusInfo{reasonCode: "cable unlocked"}
        }}, state}
end
