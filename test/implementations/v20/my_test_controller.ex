defmodule Implementations.V20.MyTestController do
  alias OcppModel.V20.Behaviours, as: B
  alias OcppModel.V20.Messages, as: M

  @behaviour B.Controller

  def handle([2, id, action, payload], state) do
    case B.Controller.handle(__MODULE__, action, payload, state) do
      {{:ok, response_payload}, new_state} -> {[3, id, response_payload], new_state}
      {{:error, error, desc}, new_state} -> {[4, id, Atom.to_string(error), desc, {}], new_state}
    end
  end

  def handle([3, id, payload], _state),
    do: IO.puts("Received answer for id #{id}: #{inspect(payload)}")

  def handle([4, id, err, desc, det], _state),
    do: IO.puts("Received error for id #{id}: #{err}, #{desc}, #{det}")

  def publish_firmware(_req, state) do
    {{:ok, %M.PublishFirmwareResponse{status: "Accepted"}}, state}
  end
end
